package org.example.book.persistence.entity;

public enum BookCategory {
    COMIC,
    CRIME,
    CLASSIC,
    FANTASY,
    SHORT_STORY,
    HISTORY,
    NOVEL;
}
