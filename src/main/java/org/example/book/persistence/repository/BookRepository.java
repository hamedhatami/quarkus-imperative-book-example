package org.example.book.persistence.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Sort;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.Response;
import org.example.book.persistence.entity.Award;
import org.example.book.persistence.entity.Book;

import java.time.Instant;
import java.util.List;
import java.util.Map;

@ApplicationScoped
@Transactional
public class BookRepository implements PanacheRepository<Book> {

    public Response getBookList() {
        try {
            final var books = listAll(Sort.by("title"));
            return Response.ok(books).build();
        } catch (Exception e) {
            throw new IllegalStateException("Error : " + e.getMessage());
        }
    }

    public Response addBook(final Book book) {
        try {
            book.setAward(getAwards());
            persist(book);
            return Response.ok("book is added").build();
        } catch (Exception e) {
            throw new IllegalStateException("Error : " + e.getMessage());
        }
    }

    public Response updateBook(final Book book,
                               final String id) {
        try {
            update("author = :author , publisher = :publisher , title = :title , category = :category where id = :id",
                    Map.of("author", book.getAuthor(),
                            "publisher", book.getPublisher(),
                            "title", book.getTitle(),
                            "category", book.getCategory(),
                            "id", id));
            return Response.ok("book is updated").build();
        } catch (Exception e) {
            throw new IllegalStateException("Error : " + e.getMessage());
        }

    }

    public Response deleteBook(final String id) {
        try {
            final var deleted = deleteById(Long.valueOf(id));
            return Response.ok(deleted).build();
        } catch (Exception e) {
            throw new IllegalStateException("Error : " + e.getMessage());
        }
    }

    private List<Award> getAwards() {
        return List.of(new Award("1", Instant.now(), 1),
                new Award("2", Instant.now(), 2),
                new Award("3", Instant.now(), 3),
                new Award("4", Instant.now(), 4),
                new Award("5", Instant.now(), 5));
    }
}
