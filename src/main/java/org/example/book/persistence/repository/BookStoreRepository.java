package org.example.book.persistence.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Sort;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.Response;
import org.example.book.persistence.entity.BookStore;

import java.util.UUID;

@ApplicationScoped
@Transactional
public class BookStoreRepository implements PanacheRepository<BookStore> {

    @Inject
    BookRepository bookRepository;

    public Response getBookStoreList() {
        try {
            final var bookStores = listAll(Sort.by("name"));
            return Response.ok(bookStores).build();
        } catch (Exception e) {
            throw new IllegalStateException("Error : " + e.getMessage());
        }
    }

    public Response addBookStore(final BookStore bookStore) {
        try {
            persist(bookStore);
            return Response.ok("bookStore is added").build();
        } catch (Exception e) {
            throw new IllegalStateException("Error : " + e.getMessage());
        }
    }

    public Response addBookToBookStore(final UUID bookId,
                                       final UUID bookStoreId) {
        try {
            final var bookStore = find("id", bookStoreId).singleResult();
            final var book = bookRepository.find("id", bookId).singleResult();
            bookStore.getBooks().add(book);
            persist(bookStore);
            return Response.ok("{\"assigned\":\"true\"}").build();
        } catch (Exception e) {
            throw new IllegalStateException("Error : " + e.getMessage());
        }
    }
}
